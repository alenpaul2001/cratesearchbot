mod cratesio;
mod handlers;

use grammers_client::{Client, Config, InitParams};
use grammers_session::Session;
use log;
use tokio::{runtime, task};

use handlers::handle_update;
use std::env;

type Result = std::result::Result<(), Box<dyn std::error::Error>>;
const SESSION_FILE: &str = "echo.session";

async fn async_main() -> Result {
    let api_id = env::var("API_ID").unwrap().parse().expect("INVALID TOKEN");
    let api_hash = env::var("API_HASH").unwrap().to_string();
    let token = env::var("BOT_TOKEN").expect("INVALID BOTTOKEN").to_string();

    log::info!("Connecting to Telegram...");
    let mut client = Client::connect(Config {
        session: Session::load_file_or_create(SESSION_FILE).unwrap(),
        api_id,
        api_hash: api_hash.clone(),
        params: InitParams {
            ..Default::default()
        },
    })
    .await?;
    log::info!("Connected!");

    if !client.is_authorized().await? {
        log::info!("Signing in...");
        client.bot_sign_in(&token, api_id, &api_hash).await?;
        client.session().save_to_file(SESSION_FILE)?;
        log::info!("Signed in!");
    }

    log::info!("Waiting for messages...");

    while let Some(update) = tokio::select! {
        _ = tokio::signal::ctrl_c() => Ok(None),
        result = client.next_update() => result,
    }? {
        let handle = client.clone();
        task::spawn(async move {
            match handle_update(handle, update).await {
                Ok(_) => {}
                Err(e) => log::error!("Error handling updates!: {}", e),
            }
        });
    }

    log::info!("Saving session file and exiting...");
    client.session().save_to_file(SESSION_FILE)?;
    Ok(())
}

fn main() -> Result {
    let runtime = runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap();
    runtime.block_on(async_main())?;
    Ok(())
}
