use crate::cratesio::generate_result;
use grammers_client::{
    button, reply_markup,
    types::{Chat, Message},
    Client, InputMessage, Update,
};

type Result = std::result::Result<(), Box<dyn std::error::Error>>;

pub async fn handle_update(_: Client, update: Update) -> Result {
    match update {
        Update::NewMessage(message) => {
            if filter_incoming(&message) && filter_command(&message, "/start") {
                message
                    .reply(
                        InputMessage::html(
                            "This bot searches crates from <a href='https://crates.io/'>\
                            Rust Package Registry</a>, works only in inline mode \
                            Inspired from @FDroidSearchBot\n\n\
                            <a href='https://gitlab.com/alenpaul2001/cratesearchbot'>Source Code</a> | \
                            <a href='https://t.me/bytesio'>Developer</a> | \
                            <a href='https://t.me/bytessupport'>Support Chat</a>",
                        )
                        .reply_markup(&reply_markup::inline(vec![vec![
                            button::switch_inline("Search Packages", ""),
                        ]])),
                    )
                    .await?;
            }
        }
        Update::InlineQuery(inline) => {
            if inline.text().is_empty() {
                inline
                    .answer([])
                    .switch_pm("Type to search crates", "start")
                    .send()
                    .await?;
                return Ok(());
            }
            let mut offset = inline.offset().parse::<u64>().unwrap_or_default();
            offset += 1;
            let result = generate_result(inline.text(), offset).await;
            inline.answer(result).next_offset("20").send().await?;
        }
        _ => {}
    };
    Ok(())
}

fn filter_incoming(message: &Message) -> bool {
    let sender = message.sender();
    if sender.is_none() {
        return false;
    }
    if let Chat::User(user) = sender.unwrap() {
        !user.is_self()
    } else {
        false
    }
}

fn filter_command(message: &Message, command: &str) -> bool {
    message.text().starts_with(command)
}
