use chrono::Utc;
use crates_io_api::{AsyncClient, Crate, CratesQuery, Sort};
use grammers_client::types::inline_query::{Article, InlineResult};
use grammers_client::InputMessage;
use lazy_static::lazy_static;

lazy_static! {
    static ref CRATECLIENT: AsyncClient = AsyncClient::new(
        "CrateSearchBot (no@email.com)",
        std::time::Duration::from_millis(1500)
    )
    .unwrap();
}

pub async fn generate_result(query: &str, offset: u64) -> Vec<InlineResult> {
    let mut cquery = CratesQuery::builder()
        .search(query)
        .page_size(50)
        .sort(Sort::Relevance)
        .build();
    cquery.set_page(offset);
    let result = CRATECLIENT.crates(cquery).await.unwrap();
    let result = result.crates.into_iter().map(|pack| {
        Article::new(pack.name.clone(), input_message_builder(&pack))
            .description(pack.description.unwrap_or_default())
            .into()
    });
    result.collect()
}

fn input_message_builder(pack: &Crate) -> InputMessage {
    InputMessage::html(format!(
        "📦 <b>{}\t{}</b>\n\n\
    ℹ️{}\n\
    🔗<a href='{}'>Git</a> | \
    <a href='{}'>Doc</a> | \
    <a href='{}'>HomePage</a>\n\
    - Maintainer: <code>{}</code>\n\
    - Downloads: <code>{}</code>\n\
    - Last Updated: <code>{} days ago</code>\n\
    - First Submitted: <code>{}</code>
    ",
        pack.name,
        pack.max_version,
        pack.description.as_ref().unwrap_or(&String::from("None")),
        pack.repository.as_ref().unwrap_or(&String::from("None")),
        pack.documentation.as_ref().unwrap_or(&String::from("None")),
        pack.homepage.as_ref().unwrap_or(&String::from("None")),
        pack.id,
        pack.downloads,
        pack.updated_at
            .signed_duration_since(Utc::now())
            .num_days()
            .abs(),
        pack.created_at.format("%Y-%m-%d %H:%M")
    ))
}
