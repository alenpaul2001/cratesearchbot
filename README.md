# CrateSearchBot

[![made-with-rust](https://img.shields.io/badge/Made%20with-Rust-1f425f.svg)](https://www.rust-lang.org/)
[![LICENSE](https://img.shields.io/github/license/alenpaul2001/CrateSearchBot?color=%2340AA8B)](./LICENSE.md)
[![Try it on telegram](https://img.shields.io/badge/try%20it-on%20telegram-0088cc.svg)](http://t.me/CrateSearchBot)

A Telegram Inline Search Bot Written in Rust

# Introduction

Telegram Bot that can search [Rust Package Registry](https://crates.io/) ( crate.io ) in inline mode. This bot make use of crate.io [api](https://crates.io/data-access) to find packages.


### Building & Running

build using `cargo build`
```sh
git clone https://gitlab.com/alenpaul2001/cratesearchbot.git
cd cratesearchbot
cargo build --release
```

set environment variable `API_ID` and `API_HASH` and `BOT_TOKEN` 
which you can get from https://my.telegram.org/auth and @Botfather then run the target binary.
alternatively you can use cargo run<br>

### Copyright & License 

* Copyright (C) 2022 by [AlenPaulVarghese](https://github.com/alenpaul2001)
* Licensed under the terms of the [BSD 3-Clause License](./LICENSE.md)
